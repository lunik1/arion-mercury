{ config, pkgs, lib, ... }:

with lib;

let
  secrets = import ./secrets.nix;

  arion-utils' = pkgs.arion-utils.lib {
    puid = secrets.PUID;
    pgid = secrets.PGID;
    domain = secrets.domain;
    httpUser = secrets.http.user;
    httpPass = secrets.http.pass;
  };
in
with arion-utils';
{
  config = {
    project.name = "matrix";

    docker-compose.volumes = {
      traefik = { };
      atuin = { };
      thelounge = { };
      rss-db = { };
      rss-db-backup = { };
      wallabag-images = { };
      matrix-db = { };
      matrix-db-backup = { };
      synapse = { };
      syncthing = { };
      ledger = { };
      boinc = { };
    };

    services = {
      traefik = mkService {
        service =
          let
            sftpPort = 2222;
            sftpPortStr = toString sftpPort;
          in
          {
            image = "traefik:latest";
            container_name = "traefik";
            command = [
              "--entrypoints.web.address=:80"
              "--entrypoints.websecure.address=:443"
              "--entrypoints.sftp.address=:${sftpPortStr}/tcp"
              "--providers.docker"
              "--providers.file.filename=/etc/traefik/dynamic.yml"
              "--api"
              "--api.dashboard=true"
              "--certificatesresolvers.le.acme.email=xx.acme@themaw.xyz"
              "--certificatesresolvers.le.acme.storage=/config/acme.json"
              "--certificatesresolvers.le.acme.tlschallenge=true"

              "--accessLog.filePath=/var/log/access.log"
              "--accessLog.filters.statusCodes=400-499"
              # global https redirect
              "--entrypoints.web.http.redirections.entryPoint.to=websecure"
              "--entrypoints.web.http.redirections.entryPoint.scheme=https"
            ];
            ports = [ "80:80" "443:443" "${sftpPortStr}:${sftpPortStr}" ];
            volumes = [
              "traefik:/config" # acme.json store
              rec {
                type = "bind";
                source = "/var/run/docker.sock";
                target = source;
                read_only = true;
              }
              "${
              builtins.toFile "traefik-dynamic.yml" (builtins.toJSON {
                tls.options.default = {
                  minVersion = "VersionTLS13";
                  sniStrict = true;
                };
              })
            }:/etc/traefik/dynamic.yml:ro"
            ];
            labels = {
              "traefik.enable" = "true";
              "traefik.http.routers.dashboard.rule" =
                "Host(`traefik.${secrets.domain}`)";
              "traefik.http.routers.dashboard.service" = "api@internal";
              "traefik.http.routers.dashboard.middlewares" = "auth,compression";
              "traefik.http.middlewares.compression.compress" = "true";
              "traefik.http.middlewares.auth.basicauth.users" =
                "${secrets.http.user}:${escapeDockerCompose secrets.http.pass}";
              "traefik.http.routers.dashboard.tls.certresolver" = "le";
              "traefik.http.routers.dashboard.entrypoints" = "websecure";
            };
          };
      };

      munin = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            services.darkhttpd = {
              enable = true;
              rootDir = "/srv";
              address = "0.0.0.0";
            };
            systemd.services.darkhttpd.serviceConfig.DynamicUser = lib.mkForce false;
            networking.enableIPv6 = false;
          };
        };
        service = with config.nixos.evaluatedConfig.services.darkhttpd; rec {
          container_name = "munin";
          volumes = [ "/var/www/munin:${rootDir}" ];
          labels = mkTraefikLabels {
            service = container_name;
            port = port;
          };
        };
      });

      ## Syncthing
      syncthing =
        let webUiPort = 8384;
        in
        mkNixosService ({ config, ... }: {
          nixos = {
            configuration = { config, ... }: {
              services.syncthing = {
                enable = true;
                guiAddress = "0.0.0.0:${toString webUiPort}";
              };
              systemd.services.fix-syncthing-permissions =
                with config.services.syncthing; {
                  description = "Set permissions on syncthing's dataDir";
                  requiredBy = [ "syncthing.service" ];
                  before = [ "syncthing.service" ];
                  serviceConfig = {
                    ExecStart =
                      "${pkgs.coreutils}/bin/chown -R ${user}:${group} ${dataDir}";
                    Type = "oneshot";
                    RemainAfterExit = true;
                  };
                };
            };
          };
          service = with config.nixos.evaluatedConfig.services.syncthing; rec {
            container_name = "syncthing";
            ports = [ "22000:22000" "21027:21027/udp" ];
            volumes = [
              "syncthing:${dataDir}"
              "ledger:${dataDir}/ledger"
            ];
            labels = mkTraefikLabels {
              service = container_name;
              port = webUiPort;
            };
          };
        });

      ## Fava
      fava = mkNixosService {
        nixos = {
          configuration = {
            systemd.services.fava = {
              description = "Fava Web UI for Beancount";
              after = [ "network.target" ];
              wantedBy = [ "multi-user.target" ];
              serviceConfig = {
                Type = "simple";
                ExecStart =
                  "${pkgs.fava}/bin/fava --host 0.0.0.0 /ledger/ledger.beancount";
              };
            };
          };
        };
        service = rec {
          container_name = "fava";
          volumes = [ "ledger:/ledger" ];
          labels = mkTraefikLabels {
            service = container_name;
            port = 5000;
          };
        };
      };

      ## Privacy frontends

      quetre = mkService {
        service = rec {
          image = "codeberg.org/video-prize-ranch/quetre";
          container_name = "quetre";
          labels = mkTraefikLabels {
            service = container_name;
            port = 3000;
          };
        };
      };

      ## IRC stuff

      # Lounge users will need to be manually added
      thelounge = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            services.thelounge = {
              enable = true;
              plugins = with pkgs.theLoungePlugins; [ themes.zenburn ];
              extraConfig = {
                prefetch = true;
                prefetchStorage = true;
                prefetchMaxImageSize = 10240;
                maxHistory = 5000;
                leaveMessage = ":x";
                identd = {
                  enable = true;
                  port = 9001;
                };
              };
            };
          };
        };
        service = rec {
          container_name = "thelounge";
          volumes = [
            "thelounge:/var/lib/thelounge" # logs etc.
          ];
          labels = mkTraefikLabels {
            service = container_name;
            port = config.nixos.evaluatedConfig.services.thelounge.port;
            auth = false;
          };
        };
      });

      ## RSS stuff

      redis = mkNixosService {
        nixos = {
          configuration = {
            services.redis = {
              servers."" = {
                enable = true;
                bind = "0.0.0.0";
                settings.protected-mode = "no";
              };
              # vmOverCommit = true; # no effect in container
            };
          };
        };

        service.container_name = "redis";
      };

      # https://github.com/DIYgod/RSSHub/blob/master/docker-compose.yml
      chrome-headless = mkService {
        service = {
          image = "browserless/chrome";
          container_name = "chrome-headless";
          tmpfs = mkForce [ "/tmp:exec,mode=777" ]; # fix chown error on /run/shm
        };
      };

      rsshub = mkService {
        service = {
          image = "diygod/rsshub";
          container_name = "rsshub";
          environment = with secrets.rsshub; {
            PORT = 80;
            NODE_ENV = "production";
            CACHE_TYPE = "redis";
            REDIS_URL = "redis://redis:6379/";
            PUPPETEER_WS_ENDPOINT = "ws://chrome-headless:3000";

            # Twitter API access
            TWITTER_CONSUMER_KEY = twitter.consumer-key;
            TWITTER_CONSUMER_SECRET = twitter.consumer-key-secret;
            TWITTER_TOKEN_lunik1 = with twitter;
              "${consumer-key},${consumer-key-secret},${access-token},${access-token-secret}";

            # Instagram hub
            IG_USERNAME = instagram.user;
            IG_PASSWORD = instagram.pass;
          };
          depends_on = [ "redis" "chrome-headless" ];
        };
      };

      mercury = mkService {
        service = {
          image = "wangqiru/mercury-parser-api";
          container_name = "mercury";
        };
      };

      rss-db = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [
              pgcli
              postgresql_14_jit
            ];
            i18n.defaultLocale = mkForce "en_US.UTF-8";
            services = {
              postgresql = {
                enable = true;
                package = pkgs.postgresql_14_jit;
                enableTCPIP = true;
                initialScript = with secrets;
                  pkgs.writeText "postgres-init.sql" ''
                    -- tt-rss DB
                    CREATE ROLE "${tt-rss.db.user}" WITH LOGIN PASSWORD '${tt-rss.db.pass}';
                    CREATE DATABASE "${tt-rss.db.name}" WITH OWNER "${tt-rss.db.user}";

                    -- wallabag DB
                    CREATE ROLE "${wallabag.db.user}" WITH LOGIN PASSWORD '${wallabag.db.pass}';
                    CREATE DATABASE "${wallabag.db.name}" WITH OWNER "${wallabag.db.user}";
                  '';
                authentication = mkForce ''
                  # TYPE  DATABASE        USER            ADDRESS                 METHOD
                  # localhost
                  local   all             all                                     trust
                  host    all             all             127.0.0.1/32            trust
                  host    all             all             ::1/128                 trust
                  # podman containers
                  host    all             all             10.0.0.0/8              trust
                '';
              };
              postgresqlBackup = {
                enable = true;
                startAt = "02:00:00";
                databases = [ "tt-rss" "wallabag" ];
                compression = "zstd";
              };
            };
          };
        };

        service = {
          container_name = "rss-db";
          volumes = with config.nixos.evaluatedConfig.services; [
            "rss-db:${postgresql.dataDir}"
            "rss-db-backup:${postgresqlBackup.location}"
          ];
        };
      });

      # podman exec wallabag /var/www/wallabag/bin/console wallabag:install --env=prod --no-interaction
      # must be run when first setting up wallabag
      wallabag = mkService {
        service = rec {
          image = "wallabag/wallabag";
          container_name = "wallabag";
          environment = with secrets; {
            POSTGRES_USER = wallabag.db.name;
            POSTGRES_PASSWORD = wallabag.db.user;
            SYMFONY__ENV__DATABASE_HOST = "rss-db";
            SYMFONY__ENV__DATABASE_DRIVER = "pdo_pgsql";
            SYMFONY__ENV__DATABASE_PORT = 5432;
            SYMFONY__ENV__DATABASE_NAME = wallabag.db.name;
            SYMFONY__ENV__DATABASE_USER = wallabag.db.user;
            SYMFONY__ENV__DATABASE_PASSWORD = wallabag.db.pass;
            SYMFONY__ENV__DOMAIN_NAME = "https://wallabag.${secrets.domain}";
          };
          volumes = [ "wallabag-images:/var/www/wallabag/web/assets/images" ];
          depends_on = [ "rss-db" "redis" ];
          labels = mkTraefikLabels {
            service = container_name;
            auth = false;
            port = 80;
          };
        };
      };

      tt-rss = mkService {
        service = rec {
          image = "wangqiru/ttrss";
          container_name = "tt-rss";
          environment = with secrets; {
            DB_HOST = "rss-db";
            DB_PORT = 5432;
            CHECK_FOR_UPDATES = "false";
            SELF_URL_PATH = "https://tt-rss.${domain}/";
            SINGLE_USER_MODE = "true";
            DB_NAME = tt-rss.db.name;
            DB_USER = tt-rss.db.user;
            DB_PASS = tt-rss.db.pass;
            ENABLE_PLUGINS = "auth_internal,wallabag_v2,mercury_fulltext";
          };
          depends_on = [ "rss-db" "rsshub" ];
          labels = mkTraefikLabels {
            service = container_name;
            port = 80;
          };
        };
      };

      ## Matrix stuff

      well-known = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            services.darkhttpd = {
              enable = true;
              rootDir = "/www";
              address = "0.0.0.0";
            };
            networking.enableIPv6 = false;
            systemd.services.darkhttpd.serviceConfig.DynamicUser = lib.mkForce false;
            systemd.tmpfiles.rules = [
              "L+ /www/.well-known/matrix/server - - - - ${
                builtins.toFile "server" (builtins.toJSON {
                  "m.server" = "synapse.${secrets.domain}:443";
                })
              }"
              "L+ /www/.well-known/matrix/client - - - - ${
                builtins.toFile "client" (builtins.toJSON {
                  "m.homeserver".base_url = "https://${secrets.domain}";
                })
              }"
            ];
          };
        };
        service = rec {
          container_name = "well-known";
          labels = mkTraefikLabels {
            service = container_name;
            auth = false;
            host = secrets.domain;
            port = config.nixos.evaluatedConfig.services.darkhttpd.port;
          };
        };
      });

      matrix-db = mkNixosService ({ config, ... }: {
        nixos = {
          configuration = {
            environment.systemPackages = with pkgs; [
              matrix-synapse-tools.rust-synapse-compress-state
              pgcli
              postgresql_13_jit
            ];
            i18n.defaultLocale = mkForce "en_US.UTF-8";
            services = {
              postgresql = {
                enable = true;
                package = pkgs.postgresql_13_jit;
                enableTCPIP = true;
                port = 5432;
                initialScript = with secrets;
                  pkgs.writeText "postgres-init.sql" ''
                    -- Synapse DB
                    CREATE ROLE "${synapse.db.user}" WITH LOGIN PASSWORD '${synapse.db.pass}';
                    CREATE DATABASE "${synapse.db.name}" WITH OWNER "${synapse.db.user}"
                      TEMPLATE template0
                      LC_COLLATE = "C"
                      LC_CTYPE = "C";
                  '';
                authentication = mkForce ''
                  # Generated file; do not edit!
                  # Trust local connections (IPv4 and IPv6)
                  # TYPE  DATABASE        USER            ADDRESS                 METHOD
                  local   all             all                                     trust
                  host    all             all             127.0.0.1/32            trust
                  host    all             all             ::1/128                 trust
                  # and those from docker containers
                  host    all             all             172.16.0.0/12           trust
                  # and those from podman containers
                  host    all             all             10.0.0.0/8              trust
                '';
              };
              postgresqlBackup = {
                enable = true;
                startAt = "02:00:00";
                databases = [ "synapse" ];
                compression = "zstd";
              };
            };
            systemd.services = {
              synapse-auto-compressor = with secrets.synapse; {
                description = "Compress synapse database";
                requires = [ "postgresql.service" ];
                after = [ "postgresql.service" ];
                startAt = "Sat *-*-8..14 04:47:00"; # second saturday of the month @ 04:47 am
                serviceConfig = {
                  ExecStart =
                    "${pkgs.matrix-synapse-tools.rust-synapse-compress-state}/bin/synapse_auto_compressor -p postgresql://${db.user}:${db.pass}@localhost/${db.name} -c 2000 -n 500";
                  Type = "oneshot";
                  Nice = 15;
                  IOSchedulingPriority = 7;
                  CPUSchedulingPolicy = "batch";
                };
              };
            };
          };
        };

        service = {
          container_name = "matrix-db";
          volumes = [
            "matrix-db:${config.nixos.evaluatedConfig.services.postgresql.dataDir}"
            "matrix-db-backup:${config.nixos.evaluatedConfig.services.postgresqlBackup.location}"
          ];
          labels = { "traefik.enable" = "false"; };
        };
      });

      synapse =
        let
          dataDir = "/data";
          moduleConfig = config;
        in
        mkNixosService ({ config, ... }: {
          nixos = {
            configuration = {
              environment.systemPackages = with pkgs; [ matrix-synapse ];
              environment.noXlibs = lib.mkForce false;
              services.matrix-synapse = {
                inherit dataDir;
                enable = true;
                withJemalloc = true;
                settings = {
                  server_name = secrets.domain;
                  public_baseurl = "https://synapse.${secrets.domain}";
                  max_upload_size = "100M";
                  max_image_pixels = "64M";
                  listeners = [{
                    port = 8008;
                    type = "http";
                    tls = false;
                    x_forwarded = true;
                    bind_addresses = [ "0.0.0.0" ];
                    resources = [{
                      names = [ "client" "federation" ];
                      compress = false;
                    }];
                  }];
                  enable_registration = false; # true for open registration
                  database.args = with secrets.synapse.db; {
                    user = user;
                    password = pass;
                    database = name;
                    host = moduleConfig.services.matrix-db.service.container_name;
                  };
                  experimenal_features.spaces_enabled = true;
                  suppress_key_server_warning = true;
                  trusted_key_servers = [
                    {
                      server_name = "matrix.org";
                      verify_keys = { "ed25519:auto" = "Noi6WqcDj0QmPxCNQqgezwTlBKrfqehY1u2FyWP9uYw"; };
                    }
                    {
                      server_name = "nixos.org";
                      verify_keys = { "ed25519:j8tsLm" = "ysJrOC8kica9QA/fOCQT/lHJvcyCDnr1lCvXN0wsxwA"; };
                    }
                    {
                      server_name = "mozilla.org";
                      verify_keys = { "ed25519:0" = "RsDggkM9GntoPcYySc8AsjvGoD0LVz5Ru/B/o5hV9h4"; };
                    }
                  ];
                  log_config = builtins.toFile "log-config.yaml" ''
                    version: 1
                    formatters:
                        journal_fmt:
                            format: '%(name)s: [%(request)s] %(message)s'
                    filters:
                        context:
                            (): synapse.util.logcontext.LoggingContextFilter
                            request: ""
                    handlers:
                        journal:
                            class: systemd.journal.JournalHandler
                            formatter: journal_fmt
                            filters: [context]
                            SYSLOG_IDENTIFIER: synapse
                    root:
                        level: WARNING
                        handlers: [journal]
                    disable_existing_loggers: False
                  '';
                };
              };

              ## Needed when registering new users, see Chap 24 of NixOS manual
              ## TLDR: uncomment lines below and use
              ## docker exec -it synapse register_new_matrix_user -k <registration_shared_secret> http://localhost:8008
              ## or for open registration set enable_registration above to true
              # configuration.environment.systemPackages = [ pkgs.matrix-synapse ];
              # configuration.services.matrix-synapse.registration_shared_secret =
              #   secrets.synapse.registration_shared_secret;
            };
          };

          service = rec {
            container_name = "synapse";
            volumes = [ "synapse:${dataDir}" ];
            labels = mkTraefikLabels {
              service = container_name;
              auth = false;
              port = (builtins.head config.nixos.evaluatedConfig.services.matrix-synapse.settings.listeners).port;
            };
            depends_on = [ "well-known" "matrix-db" ];
          };
        });

      atuin =
        mkNixosService ({ config, ... }: {
          nixos = {
            configuration = {
              environment.noXlibs = lib.mkForce false;
              services.postgresql = {
                package = pkgs.postgresql_16_jit;
                enable = true;
                ensureUsers = [{
                  name = "root";
                  ensureDBOwnership = true;
                }];
                ensureDatabases = [ "root" ];
              };
              services.atuin = {
                host = "0.0.0.0";
                enable = true;
                openFirewall = true;
                openRegistration = false;
                database = {
                  createLocally = false;
                  uri = "postgresql:///root?host=/run/postgresql";
                };
              };
              systemd.services.atuin = {
                after = [ "network.service" "postgresql.service" ];
                serviceConfig.DynamicUser = lib.mkForce false;
              };
            };
          };
          service = with config.nixos.evaluatedConfig.services.atuin; rec {
            capabilities.SYS_ADMIN = true;
            container_name = "atuin";
            volumes = [
              "atuin:/var/lib/postgresql"
            ];
            labels = mkTraefikLabels {
              service = container_name;
              inherit port;
              auth = false;
            };
          };
        });

      boinc = mkLsioService {
        name = "boinc";
        publish = true;
        port = 8080;
        extraConfig = {
          service.tmpfs = mkForce [ "/tmp:exec,mode=777" ];
          out.service = { security_opt = [ "seccomp:unconfined" ]; };
        };
      };
    };
  };
}
